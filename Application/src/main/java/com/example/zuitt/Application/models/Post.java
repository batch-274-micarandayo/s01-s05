package com.example.zuitt.Application.models;

import javax.persistence.*;

/*mark this java object as a representation of db table via @Entity*/
@Entity
/*creation or designation of table name via @Table*/
@Table(name="posts")
public class Post {
    /*indicate that this property represents the primary key via @id*/
    @Id
    /*values for this property will be auto-incremented*/

    @GeneratedValue
    private Long id;

    @Column
    private String title;

    @Column
    private String content;

    /*Default constructor, this is needed when retrieving posts*/
    public Post(){}

    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
